#!/bin/bash

IFS=','; NAMES=($1); unset IFS;

SAN=''
CANONICAL=''
for NAME in "${NAMES[@]}"
do
    if [[ -z $CANONICAL ]]
    then
        CANONICAL=${NAMES[0]}
        SAN="DNS:$NAME"
    else
        SAN="${SAN},DNS:$NAME"
    fi
done

openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -extensions san \
    -subj "/CN=$CANONICAL" -keyout $CANONICAL.key -out $CANONICAL.crt \
    -config <( echo "[req]";echo "distinguished_name=req";echo "[san]";echo "subjectAltName=$SAN" )

cat $CANONICAL.key $CANONICAL.crt > zzz-${CANONICAL}.pem

sudo mkdir -p /etc/ssl/certs/haproxy/
sudo cp zzz-${CANONICAL}.pem /etc/ssl/certs/haproxy/
sudo chmod 0600 /etc/ssl/certs/haproxy/zzz-${CANONICAL}.pem
