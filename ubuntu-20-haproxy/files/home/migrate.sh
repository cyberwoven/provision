#!/bin/bash

migrate() {
    local DOMAIN=$1
    local DBNAME=$2

    echo "*** ${DOMAIN}: Copying public files..."
    mkdir -p /var/www/vhosts/${DOMAIN}/pub/sites/default/files
    pushd /var/www/vhosts/${DOMAIN}/pub/sites/default/files
        sudo chown -R cyberwoven. .
        rsync -vcrz old:/var/www/vhosts/${DOMAIN}/pub/sites/default/files/ . --exclude={css,js,styles,php}
        sudo chown -R www-data. .
    popd

    echo "*** ${DOMAIN}: Copying private files..."
    mkdir -p /var/www/vhosts/${DOMAIN}/private_files
    pushd /var/www/vhosts/${DOMAIN}/private_files
        sudo chown -R cyberwoven. .
        rsync -vcrz old:/var/www/vhosts/${DOMAIN}/private_files/ .
        sudo chown -R www-data. .
    popd

    echo "*** ${DOMAIN}: Dumping & importing database..."
    (ssh old "mysqldump ${DBNAME} | gzip") | gzip -d | mysql ${DBNAME}

    echo "*** ${DOMAIN}: Clearing cache"
    drush --root=/var/www/vhosts/${DOMAIN}/pub cr
}

migrate "www.example.com" "exampledb"
migrate "www.otherexample.com" "otherdb"

# tweak this script according to your needs, e.g., D7 has a different 
# cache-clear command, private_files might live elsewhere, there might
# be a D7 site and a D8 site, or multiple databases, etc.
