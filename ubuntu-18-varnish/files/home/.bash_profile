umask 0002

# Make sure global composer tools work
export PATH="$PATH:$HOME/bin:$HOME/.composer/vendor/bin"

if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

# Prep Drupal console
#. ~/.console/console.rc 2>/dev/null

# Include Drush completion.
. ~/.drush/drush.complete.sh

# Include Drush bash customizations.
. ~/.drush/drush.bashrc
