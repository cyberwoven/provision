#!/bin/bash

#
# We'll need to sudo for writing apache configs, so go ahead and make sure that's doable
#
[[ "$EUID" -ne 0 ]] || {
    echo " ✗ Don't run this as sudo, it'll mess up file ownership.";
    exit 1;
}

# this will prompt for sudo password if it's not already active
sudo true

CANONICAL=$1

if [[ $CANONICAL == www.* ]]
then
    VANITY=${CANONICAL:4}
else
    VANITY="www.$CANONICAL"
fi

bold=$(tput bold)
unbold=$(tput sgr0)

echo "We're about to create a new virtualhost as follows..."
echo
echo " Canonical (primary domain):      ${bold}$CANONICAL${unbold}"
echo " Vanity (redirects to canonical): ${bold}$VANITY${unbold}"
echo
read -r -p "Continue? [Y|n] " response
response=${response,,} # tolower
if [[ $response =~ ^(no|n) ]]; then
    exit 1
fi

##
## Create the self-signed cert, which will be used until we get the real cert
##
sudo mkdir -p /etc/apache2/self-signed-certs/
pushd /etc/apache2/self-signed-certs/ > /dev/null
    sudo bash ~/bin/self-signed.sh ${CANONICAL},${VANITY}
    echo " - Created self-signed key & certificate in /etc/apache2/self-signed-certs/";
popd

##
## Create Apache vhost config files from templates
##
## 1. /etc/apache2/sites-available/CANONICAL.conf
## 2. /etc/apache2/includes/CANONICAL.tls.conf
## 3. /etc/varnish/CANONICAL.vcl
##
sed "s/%CANONICAL%/${CANONICAL}/g" < /etc/apache2/templates/site.tls.conf | sudo tee /etc/apache2/includes/${CANONICAL}.tls.conf > /dev/null
sed "s/%CANONICAL%/${CANONICAL}/g" < /etc/apache2/templates/site.conf | sudo tee /etc/apache2/sites-available/${CANONICAL}.conf > /dev/null
sudo sed "s/%VANITY%/${VANITY}/g" -i /etc/apache2/sites-available/${CANONICAL}.conf > /dev/null
