#!/bin/bash

#
# We'll need to sudo for writing apache configs, so go ahead and make sure that's doable
#
[[ "$EUID" -ne 0 ]] || {
    echo " ✗ Don't run this as sudo, it'll mess up file ownership.";
    exit 1;
}

# this will prompt for sudo password if it's not already active
sudo true

CANONICAL=$1

if [[ $CANONICAL == www.* ]]
then
    VANITY=${CANONICAL:4}
else
    VANITY="www.$CANONICAL"
fi

if [[ -f /etc/apache2/sites-available/${CANONICAL}.conf ]]
then
    echo " ✗ ABORT - This site already exists.";
    exit 1;
fi

if [[ -f /etc/apache2/prelaunch-suffix.txt ]]
then
    PRELAUNCH_SUFFIX=`cat /etc/apache2/prelaunch-suffix.txt`
fi

bold=$(tput bold)
unbold=$(tput sgr0)

echo "We're about to create a new virtualhost as follows..."
echo
echo " Canonical: ${bold}${CANONICAL}${unbold}"
echo " Vanity:    ${bold}${VANITY}${unbold}"
echo " Prelaunch: ${bold}${CANONICAL}.${PRELAUNCH_SUFFIX}${unbold}"
echo
read -r -p "Continue? [Y|n] " response
response=${response,,} # tolower
if [[ $response =~ ^(no|n) ]]; then
    exit 1
fi

##
## Create the self-signed cert, which will be used until we get the real cert
##
echo "Creating temporary self-signed certificate for canonical hostname, adding to haproxy ...";
pushd /tmp > /dev/null
    sudo bash ~/bin/self-signed.sh ${CANONICAL},${VANITY}
popd

##
## Try to get a cert for the prelaunch hostname
##
echo "Attempting to issue Let's Encrypt cert for prelaunch site ...";
sudo bash ~/bin/issue-cert.sh ${CANONICAL}.${PRELAUNCH_SUFFIX}

##
## Try to get a cert for the real hostname
##
echo "Attempting to issue Let's Encrypt cert for the canonical and vanity hostnames ...";
sudo bash ~/bin/issue-cert.sh ${CANONICAL},${VANITY}

##
## Try to get a cert for the real hostname
##
echo "Convert LE certs to be used by haproxy ...";
sudo /etc/letsencrypt/renewal-hooks/deploy/haproxy-cert-conversion.sh

##
## Create Apache vhost config files from templates
##
echo "Creating Apache config for new site - /etc/apache2/sites-available/${CANONICAL}.conf ..."
sed "s/%CANONICAL%/${CANONICAL}/g" < /etc/apache2/templates/site.conf | sudo tee /etc/apache2/sites-available/${CANONICAL}.conf > /dev/null
sudo sed "s/%VANITY%/${VANITY}/g" -i /etc/apache2/sites-available/${CANONICAL}.conf > /dev/null
sudo sed "s/%PRELAUNCH_SUFFIX%/${PRELAUNCH_SUFFIX}/g" -i /etc/apache2/sites-available/${CANONICAL}.conf > /dev/null

echo "Running Apache config test ..."
sudo a2ensite ${CANONICAL}.conf

sudo apachectl -t || {
    echo " ✗ ABORT - Apache syntax error, unable to reload config. See /etc/apache2/sites-available/${CANONICAL}.conf";
    exit 1;
}

echo "Reloading Apache ..."
sudo service apache2 graceful



