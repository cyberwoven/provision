#!/bin/bash

##
## Usage: issue-cert www.example.com,example.com
##

CMD="sudo certbot certonly --standalone -d $1 --non-interactive --agree-tos --email LE_EMAIL --http-01-port=8888"
CMD_DRY_RUN="$CMD --dry-run"

if $CMD_DRY_RUN
then
    echo
    echo "Certbot dry run succeeded for $1! Issuing the cert for real now ..."
    echo
    $CMD
    
    echo "Creating cert for haproxy and reloading config ..."
    echo
    sudo bash /etc/letsencrypt/renewal-hooks/deploy/haproxy-cert-conversion.sh
else
    echo
    echo "Certbot dry run failed for $1!"
    echo    
fi
