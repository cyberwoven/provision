#!/bin/bash
  
# after successful renewal of any given cert, we need to create a monolithic 
# certfile that's just the fullchain and privkey files glued together, and 
# placed in haproxy's cert directory.
mkdir -p /etc/letsencrypt/live
pushd /etc/letsencrypt/live

    # list all directories, cut off the trailing "/"
    DOMAINS=$(ls -d */ | cut -f1 -d'/')

    for DOMAIN in ${DOMAINS}
    do
        cat ${DOMAIN}/fullchain.pem ${DOMAIN}/privkey.pem > /etc/ssl/certs/haproxy/${DOMAIN}.pem

        # make sure only root can read this, since it's got the cleartext private key in it
        chmod 0600 /etc/ssl/certs/haproxy/${DOMAIN}.pem
    done

popd

# ...then reload haproxy so it picks up the new certs
/usr/sbin/service haproxy reload
