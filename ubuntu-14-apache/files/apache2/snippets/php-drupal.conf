index index.php index.html index.htm;

gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

location ~ \..*/.*\.php$ {
    return 403;
}

location ~ ^/sites/.*/private/ {
    return 403;
}

location / {
	try_files $uri $uri/ /index.php?$query_string;
}

location @rewrite {
	rewrite ^/(.*)$ /index.php?q=$1;
}

# Don't allow direct access to PHP files in the vendor directory.
location ~ /vendor/.*\.php$ {
	deny all;
	return 404;
}

# pass the PHP scripts to php5-fpm
location ~ '\.php$|^/update.php' {
	fastcgi_split_path_info ^(.+?\.php)(|/.*)$;
	#NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
	include fastcgi_params;
	fastcgi_param SCRIPT_FILENAME $request_filename;
	fastcgi_intercept_errors on;
	fastcgi_pass unix:/var/run/php5-fpm.sock;
}

# Fighting with Styles? This little gem is amazing. For Drupal >= 7
location ~ ^/sites/.*/files/styles/ { 
	try_files $uri @rewrite;
}

# Block access to "hidden" files and directories whose names begin with a
# period. This includes directories used by version control systems such
# as Subversion or Git to store control files.
location ~ (^|/)\. {
	deny all;
}

## Replicate the Apache <FilesMatch> directive of Drupal standard
## .htaccess. Disable access to any code files. Return a 404 to curtail
## information disclosure. Hide also the text files.
location ~* ^(?:.+\.(?:htaccess|make|txt|engine|inc|info|install|module|profile|po|pot|sh|.*sql|test|theme|twig|tpl(?:\.php)?|xtmpl|yml)|code-style\.pl|/Entries.*|/Repository|/Root|/Tag|/Template)$ {
    return 404;
}

# allow letsencrypt to do domain validation
location ^~ /.well-known/ {
	allow all;
}
